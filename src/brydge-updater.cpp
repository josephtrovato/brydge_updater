#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>

#include <glib.h>


//#include "gdbus/gdbus.h"

//static GMainLoop *main_loop;
//static DBusConnection *dbus_conn;

std::vector<char> readFile(const char* filename)
{
    // open the file:
    std::ifstream file(filename, std::ios::binary);

    // Stop eating new lines in binary mode!!!
    file.unsetf(std::ios::skipws);

    // get its size:
    std::streampos fileSize;

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // reserve capacity
    std::vector<char> vec;
    vec.reserve(fileSize);

    // read the data:
    vec.insert(vec.begin(),
               std::istream_iterator<char>(file),
               std::istream_iterator<char>());

    return vec;
}

int main()
{
    std::cout << "check 1\n";
    //std::vector<char> fw_hex = readFile("test_fw.ota.bin");
    //for(std::vector<char>::iterator it = fw_hex.begin(); it != fw_hex.end(); it++)
    //{
    //    char hex_byte = *it;
    //    std::cout << hex_byte;
    //}




}
