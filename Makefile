# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

.PHONY: all
all: brydge-updater

CFLAGS = -g -Wall -std=c++11 $(shell pkg-config --cflags glib-2.0) \
                             $(shell pkg-config --cflags dbus-glib-1) \
							 $(shell pkg-config --cflags dbus-1)

LFLAGS = $(shell pkg-config --libs glib-2.0) \
 		 $(shell pkg-config --libs dbus-glib-1) \
		 $(shell pkg-config --libs dbus-1) \
		 -lbluetooth

#INCLUDES = -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include

#rule to tell make how to build brydge-updater from brydge-updater.cpp
brydge-updater: src/brydge-updater.o
	g++ src/brydge-updater.cpp $(CFLAGS) -o brydge-updater

# This rule tells make to copy hello to the binaries subdirectory,
# creating it if necessary
.PHONY: install
install:
	mkdir -p binaries
	cp -p brydge-updater binaries

# This rule tells make to delete hello and brydge-updater.o
.PHONY: clean
clean:
	rm -f brydge-updater
